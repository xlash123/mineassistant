# Resources

- [Home Assistant WebSocket API](https://developers.home-assistant.io/docs/api/websocket/)
- [Java-Websocket library](https://github.com/TooTallNate/Java-WebSocket)
- [Bukkit block data types](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/block/data/type/package-summary.html)
