# Mine Assistant

Links Home Assistant entities to Minecraft redstone blocks.

## Getting started

1. [Download](https://dev.bukkit.org/projects/mine-assistant/files) the plugin
1. Check out the [wiki](https://gitlab.com/klikini/mineassistant/-/wikis/home) for setup instructions

Want a video tutorial? Check out this video by [Mark Watt Tech](https://www.youtube.com/channel/UCQRm_z7seHnGsBiWDNEWr6A):

[![Smart Home Control with MINECRAFT](https://gitlab.com/klikini/mineassistant/-/wikis/uploads/9dbb789d96794379c64e04c030ba073c/Screen_Shot_2021-05-26_at_12.42.25.png)](https://www.youtube.com/watch?v=13asyNDh5qo "Smart Home Control with MINECRAFT")
